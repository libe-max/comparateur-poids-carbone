'use strict'

/* State stuff */
const state = {
  items: [],
  selectedItems: [],
  activeCategory: null,
  shuffling: false,
  loading: false,
  welcomeScreen: true
}

function setState (key, val) {
  state[key] = val
  switch (key) {
    case 'welcomeScreen':
      toggleWelcomeScreen(val)
      break
    case 'loading':
      toggleLoadingState(val)
      break
    case 'shuffling':
      toggleShuffleState(val)
      break
    case 'activeCategory':
      selectCategory(val)
      break
    case 'selectedItems':
      displayItems(val)
      break
    default:
  }
  return state
}

/* Init app */
// If the app is used inside an other app,
// change this value in order to correctly
// inject the icons
let pathToIcons = false 
setState('welcomeScreen', true)
setState('loading', true)
fillGauge()
$.ajax({
  method: 'GET',
  url: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQSjFvveuhexwFZdyZKoWiYUnctEQgJNFZ2AIxZaAG3bv86J0K-5zPUUwiqUvuQpiekSP51LdOFZv6m/pub?gid=516299340&single=true&output=tsv'
}).then(res => {
  const items = parse(res)
  setState('items', items)
  setState('loading', false)
}).catch(err => {
  setTimeout(window.location.reload, 2000)
})

/* Add listeners */
$('.randomize-button').on('click', handleShuffleClick)
$('.category-selector__option').on('click', handleCategoryClick)

/* User triggered functions (can set state) */
function handleShuffleClick (e) {
  e.preventDefault()
  $(this).removeClass('randomize-button_start').addClass('randomize-button_again')
  if (state.loading) return
  if (!state.activeCategory) setState('activeCategory', 'vetements')
  if (state.welcomeScreen) setState('welcomeScreen', false)
  if (state.shuffling) return
  setState('shuffling', true)
  setTimeout(() => {
    const chosenItems = chooseItems()
    setState('selectedItems', chosenItems)
    setState('shuffling', false)
  }, 500)
}

function handleCategoryClick (e) {
  e.preventDefault()
  const category = $(this).data('category')
  setState('activeCategory', category)
}

/* Programmatically triggered functions (cannot set state) */
function parse (rawData) {
  const lines = rawData.split(/\n/)
  const ret = []
  lines.forEach((line, i) => {
    if (i < 2) return
    const vals = line.split(/\t/)
    ret.push({
      category: vals[0],
      name: vals[1],
      value: parseFloat(vals[2].replace(/\,/, '.')),
      temp_icon_name: vals[3],
      icon_name: vals[4]
    })
  })
  return ret
}

function fillGauge () {
  const $gauge = $('.comparison-gauge__gauge')
  const scale = chroma.scale(['#FAC108', '#E3234A'])
  const nbOfIncrements = 50
  for (let i = 0; i < nbOfIncrements; i++) {
    const color = scale(i / (nbOfIncrements - 1)).hex()
    const $increment = $('<div class="comparison-gauge__gauge-increment"></div>')
    $increment.css({ backgroundColor: color })
    $gauge.append($increment)
  }
}

function toggleWelcomeScreen (val) {
  return val
    ? $('.wrapper').addClass('wrapper_welcome')
    : $('.wrapper_welcome').removeClass('wrapper_welcome')
}

function toggleLoadingState (val) {
  return val
    ? $('.wrapper').addClass('wrapper_loading')
    : $('.wrapper_loading').removeClass('wrapper_loading')
}

function toggleShuffleState (val) {
  if (val) {
    $('.randomize-button').css({
      opacity: 0.5,
      cursor: 'wait'
    })
    $('.compared-item').each((i, item) => {
      $(item).css({opacity: 0.5})
      wiggleWiggleWiggle($(item))
    })
  } else {
    $('.randomize-button').css({
      opacity: '',
      cursor: ''
    })
    $('.compared-item').each((i, item) => {
      $(item).css({opacity: ''})
    })
  }
  return val
    ? $('.wrapper__display-panel').addClass('wrapper__display-panel_shuffling')
    : $('.wrapper__display-panel_shuffling').removeClass('wrapper__display-panel_shuffling')
}

function wiggleWiggleWiggle ($item) {
  if (!state.shuffling) {
    $item.css({ transform: `translate(0, 0)` })
    return
  }
  $item.css({
    transform: `translate(
      ${Math.random() * 10 - 5}px,
      ${Math.random() * 10 - 5}px
    )`
  })
  setTimeout(() => wiggleWiggleWiggle($item), 10)
}

function selectCategory (category) {
  $('.category-selector__option_active')
    .removeClass('category-selector__option_active')
  if (category === null) return
  const newTarget = $(`.category-selector__option[data-category="${category}"]`)
  newTarget.addClass('category-selector__option_active')
}

function chooseItems (e) {
  if (!state.activeCategory) return
  const category = state.activeCategory
  const items = [...state.items]
    .filter(elt => elt.category === category)
  if (items.length < 2) return
  const active = state.selectedItems
  const indexes = [
    Math.floor(Math.random() * items.length),
    Math.floor(Math.random() * items.length)
  ]
  while (indexes[1] === indexes[0]) {
    indexes[1] = Math.floor(Math.random() * items.length)
  }
  return [
    items[indexes[0]],
    items[indexes[1]]
  ]
}

function displayItems (val) {
  for (let slotPos = 0; slotPos < 2; slotPos++) {
    const $slot = $('.compared-item').eq(slotPos)
    const $textSlot = $slot.find('.compared-item__text')
    const $valueSlot = $slot.find('.compared-item__value')
    const $iconSlot = $slot.find('.compared-item__icon')
    const $sidePointer = $slot.find('.compared-item__side-pointer')
    const $heightPointer = $slot.find('.compared-item__height-pointer')
    $slot.removeClass('compared-item_empty')
    $textSlot.html(val[slotPos].name)
    $valueSlot.html(
      val[slotPos].value
      + 'kg'
      + '<span class="compared-item__value-per-year">/an</span>'
    )
    const prefix = pathToIcons ? pathToIcons + '/' : './'
    $iconSlot.css({
      backgroundImage: `url(${prefix}assets/${val[slotPos].icon_name})` })
    animateComparedItemIconBackground($iconSlot, val[slotPos].value)
    animateComparedItemValueText($valueSlot, val[slotPos].value)
    animateComparedItemSidePointer($sidePointer, val[slotPos].value)
    animateComparedItemHeightPointer($heightPointer, val[slotPos].value)
  }
}

function animateComparedItemIconBackground ($iconSlot, value) {
  const startColor = $iconSlot.css('backgroundColor')
  const endColor = getColorFromValue(value).hex()
  const scale = chroma.scale([startColor, endColor])
  moveColor()
  function moveColor (i = 0) {
    const n = i + ((1 - i) / 20)
    $iconSlot.css({ backgroundColor: scale(n).hex() })
    if (n < 0.99) setTimeout(() => moveColor(n), 15)
    else return
  }
}

function animateComparedItemValueText ($valueSlot, value) {
  const startColor = $valueSlot.css('color')
  const endColor = getColorFromValue(value).hex()
  const scale = chroma.scale([startColor, endColor])
  moveColor()
  function moveColor (i = 0) {
    const n = i + ((1 - i) / 20)
    $valueSlot.css({ color: scale(n).hex() })
    if (n < 0.99) setTimeout(() => moveColor(n), 15)
    else return
  }
}

function animateComparedItemSidePointer ($pointer, value) {
  const $container = $pointer.parents('.compared-item')
  const startPosPx = parseFloat(
    $pointer.css('bottom')
      .split('px')
      .join(''))
  const containerHeightPx = $container.innerHeight()
  const startPos = 100 * startPosPx / containerHeightPx
  const $gauge = $('.comparison-gauge__gauge')
  const gaugeMarginBottom = parseFloat(
    $gauge.css('marginBottom')
      .split('px')
      .join(''))
  const gaugeMarginTop = parseFloat(
    $gauge.css('marginTop')
      .split('px')
      .join(''))
  const $lowBoundary = $('.comparison-gauge__low-boundary')
  const endRatio = (value - 1) / 99
  const endPosPx = $lowBoundary.outerHeight()
    + gaugeMarginBottom
    + endRatio
    * ($gauge.innerHeight() - gaugeMarginTop)
  const endPos = 100 * endPosPx / containerHeightPx
  movePointer()
  function movePointer (currPos = startPos) {
    const newPos = currPos + (endPos - currPos) / 5
    if (Math.abs(endPos - newPos) > 0.01) {
      $pointer.css({ bottom: `${newPos}%` })
      setTimeout(() => movePointer(newPos), 15)
    } else {
      $pointer.css({ bottom: `${endPos}%` })
    }
  }
}

function animateComparedItemHeightPointer ($pointer, value) {
  const $container = $pointer.parents('.compared-item')
  const startPosPx = parseFloat(
    $pointer.css('left')
      .split('px')
      .join(''))
  const containerWidthPx = $container.innerWidth()
  const startPos = 100 * startPosPx / containerWidthPx
  const $gauge = $('.comparison-gauge__gauge')
  const gaugeMarginLeft = parseFloat(
    $gauge.css('marginLeft')
      .split('px')
      .join(''))
  const gaugeMarginRight = parseFloat(
    $gauge.css('marginRight')
      .split('px')
      .join(''))
  const $lowBoundary = $('.comparison-gauge__low-boundary')
  const endRatio = (value - 1) / 99
  const endPosPx = $lowBoundary.outerWidth()
    + gaugeMarginLeft
    + endRatio
    * ($gauge.innerWidth() - gaugeMarginRight)
  const endPos = 100 * endPosPx / containerWidthPx
  movePointer()
  function movePointer (currPos = startPos) {
    const newPos = currPos + (endPos - currPos) / 5
    if (Math.abs(endPos - newPos) > 0.01) {
      $pointer.css({ left: `${newPos}%` })
      setTimeout(() => movePointer(newPos), 15)
    } else {
      $pointer.css({ left: `${endPos}%` })
    }
  }
}

function getColorFromValue (value) {
  const scale = chroma.scale(['#FFBF37', '#E3234A'])
  const ratio = (value - 1) / 99
  const color = scale(ratio)
  return color
}

